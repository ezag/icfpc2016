import Network.Curl
import System.Console.ArgParser
import System.Console.ArgParser.QuickParams(RawRead)
import System.Environment

import ApiKey
import Solve
import Origami
import Types

type Action = String

data Params = Params
    { action :: Action
    , endpoint :: String
    } deriving Show

paramsParser :: ParserSpec Params
paramsParser = Params
    `parsedBy` reqPos "action"
    `andBy`    optPos "" "endpoint"

main :: IO ()
main = do
    interface <- mkApp paramsParser
    runApp interface mainApp

mainApp :: Params -> IO ()
mainApp params = do
    case action params of
        "hello" -> do
            body <- apiGet $ "hello"
            putStr body
        "get" -> do
            body <- apiGet $ endpoint params
            putStr body
        "solve" -> do
            body <- apiGet ("blob/" ++ endpoint params)
            putStrLn $ solveInput body
        "squaretest" -> do
            testContents <- readFile "./problems/000010-problem.txt"
            putStrLn $ solveInput testContents
        "solvefromfile" -> do
            testContents <- readFile (endpoint params) 
            putStrLn $ solveInput testContents
        "solvefromhash" -> do
            problem <- apiGet $ ("blob/" ++ endpoint params)
            putStrLn $ solveInput problem
        "solvesubmit" -> do
            problem <- apiGet ("blob/" ++ endpoint params)
            body <- apiPost ("solution/submit") (solveInput problem)
            putStrLn body
        "makeorigami" ->
            readFile (endpoint params) >>=
            (putStrLn.show.makeOrigami.readOrigamiDesription)
        _ -> putStrLn "wut?"

apiGet :: String -> IO String
apiGet endpoint = do
    r <- curlGetResponse
        ("http://2016sv.icfpcontest.org/api/" ++ endpoint)
        [
            CurlHttpHeaders [
                "X-API-Key: " ++ apiKey,
                "Expect: "
            ],
            CurlEncoding "gzip",
            CurlFollowLocation True
        ]
    return (respBody r)

apiPost endpoint payload = do
    r <- curlMultiPost
        ("http://2016sv.icfpcontest.org/api/" ++ endpoint)
        [
            CurlHttpHeaders [
                "X-API-Key: " ++ apiKey,
                "Expect: "
            ],
            CurlEncoding "gzip",
            CurlFollowLocation True
        ]
        undefined
    return "posted"

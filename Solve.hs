module Solve where

import Origami
import Types

solveInput :: String -> String
solveInput = show . solve . readOrigamiDesription

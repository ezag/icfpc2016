
import Test.HUnit
import Test.Framework
import Test.Framework.Providers.HUnit
import Data.Monoid
import Control.Monad
import Data.Ratio
import Geometry
import Types
import Origami
import Debug.Trace
import GrahamScan
import Data.Matrix hiding (trace)


testDistance1 = TestCase $ assertEqual 
  "Distance should work correctly 1"
  (sqrt 2)
  (distance (Point 0 0) (Point 1 1))

testDistance2 = TestCase $ assertEqual
  "Distance should work correctly 2"
  1.0
  (distance (Point 0 0) (Point 1 0))

testMakeEdge = TestCase $ assertEqual
  "makeEdge finds edge"
  (Edge 0 2)
  (makeEdge (Segment (Point 10 11) (Point 12 13)) [(Point 10 11), (Point 13 13), (Point 12 13), (Point 18 14) ])

testSlice = TestCase $ assertEqual
  "slice hui vam a ne ponyatnye messagy"
  ([1, 2])
  (slice [0,1,2,3] 1 3)

testSquareArea= TestCase $ assertEqual
  "pizda ivanovna"
  (1.0)
  (area [(Point 0 0), (Point 1 0), (Point 1 1), (Point 0 1)])

testTriangleArea = TestCase $ assertEqual
  "zalupa constantinovna"
  (0.5)
  (area [(Point 0 0), (Point 1 0), (Point 2 1)])

testSquareConvex = TestCase $ assertEqual
  "innokentii ebanko"
  (True)
  (isConvex [(Point 0 0), (Point 1 0), (Point 1 1), (Point 0 1)])

testTriangleConvex = TestCase $ assertEqual
  "innokentii ebanko triangle"
  (True)
  (isConvex [(Point 0 0), (Point 100 0), (Point 0 (-100))])

testArrowNonConvex = TestCase $ assertEqual
  "shlyapa tvorozhnaya"
  (False)
  (isConvex [(Point 0 0), (Point (1%2) (1%2)), (Point 0 1), (Point 1 (1%2))])

testNonConvex = TestCase $ assertEqual
  "shlyapa tvorozhnaya"
  (False)
  (isConvex [(Point 0 0), (Point (100) (0)), (Point 100 1), (Point (1%2) 1), (Point (5001%10000) (999999%1000000))])

testPossibleTriangleFolds = TestCase $ assertEqual
  "Simple folds"
  ([(Fold (Edge 0 1) onlyFacet), (Fold (Edge 1 2) onlyFacet), (Fold (Edge 2 0) onlyFacet)])
  (possibleFolds $ Origami [(Point 0 0), (Point (1) (0)), (Point 1 1)] [(Edge 0 1), (Edge 1 2), (Edge 2 0)] [onlyFacet]  (Meta[[]]))
  where
    onlyFacet = Facet [(Edge 0 1), (Edge 1 2), (Edge 2 0)]

testGrahamScan = TestCase $ assertEqual
   "Graham scan"
   (4)
   (length $ grahamScan points)
   where
    points = [ Point 0 0, Point (1%2) (1%2), Point 0 2, Point (2%3) (2%3), Point 0 4, Point 4 4, Point 4 2, Point 4 0, Point 2 0]

testGrahamScanSquare = TestCase $ assertEqual
   "Graham scan"
   (4)
   (length $ grahamScan points)
   where
    points = [ Point 0 0, Point 1 0, Point 1 1, Point 0 1, Point 0 (-1), Point 1 (-1), Point (-1) 1, Point (-1) 0, Point (-1) (-1)]


testGrahamScanOnEdge = TestCase $ assertEqual
   "Graham scan, point on edge"
   (4)
   (length $ grahamScan points)
   where
    points = [ Point 0 0, Point 0 2, Point 2 2, Point 2 1, Point 2 0]

testMirrorDiagonal = TestCase $ assertEqual
  "mirror across main diagonal"
  (Point 0 1)
  (mirror (Segment (Point 0 0) (Point 1 1)) (Point 1 0))

testMirrorX = TestCase $ assertEqual
  "mirror across x axis"
  (Point 0 (-1))
  (mirror (Segment (Point 0 0) (Point 1 0)) (Point 0 1))

testMirrorY = TestCase $ assertEqual
  "mirror across y axis"
  (Point (-1) 0)
  (mirror (Segment (Point 0 0) (Point 0 1)) (Point 1 0))

testMirrorSlope = TestCase $ assertEqual
  "mirror across y = 2x"
  (Point ((-3)%5) (4%5))
  (mirror (Segment (Point 0 0) (Point 1 2)) (Point 1 0))

testCalculateOneFacet = TestCase $ assertEqual
  "Facets of flat square"
  ([Facet [(Edge 0 1), (Edge 1 2), (Edge 2 3), (Edge 3 0)]])
  (calculateFacets [(Point 0 0), (Point 1 0),(Point 1 1),(Point 0 1)] [(Edge 0 3), (Edge 3 2), (Edge 2 1), (Edge 1 0)])

testCalculateSimpleFacets = TestCase $ assertEqual
  "Facets of example problem from the task"
  ([
    Facet [(Edge 0 2), (Edge 2 3), (Edge 3 0)],
    Facet [(Edge 0 2), (Edge 2 1), (Edge 1 0)],
    Facet [(Edge 0 3), (Edge 3 2), (Edge 2 1), (Edge 1 0)]
   ])
  (calculateFacets [(Point 0 0), (Point 0 1),(Point 1 1),(Point 2 0)] [(Edge 0 1), (Edge 1 2), (Edge 2 3), (Edge 3 0), (Edge 0 2)])

testCalculateHalfSquareFacets = TestCase $ assertEqual
  "Facets of half square symmetric by y on with x > 0"
  ([
    Facet [Edge 0 4,Edge 4 5,Edge 5 1,Edge 1 0],
    Facet [Edge 0 4,Edge 4 5,Edge 5 1,Edge 1 2,Edge 2 3,Edge 3 0],
    Facet [Edge 0 3,Edge 3 2,Edge 2 1,Edge 1 0]
   ])
  (calculateFacets 
    [(Point 0 0), (Point (1%2) 0),(Point (1%2) (1%2)),(Point 0 (1%2)),(Point 0 ((-1)%2)),(Point (1%2) ((-1)%2))] 
    [Edge 0 1,Edge 0 3,Edge 1 2,Edge 3 2,Edge 0 4,Edge 4 5,Edge 5 1]
  )

testNotOnEdgeLine = TestCase $ assertEqual
  "hui hui"
  (True)
  (isOnLine (Segment (Point 0 0) (Point 1 1)) (Point 2 2))

testFindTransformShift = TestCase $ assertEqual
  "hui hui"
  (fromList 3 3 [0, 0, 0, 0, 1, 0, 1%2, 0, 1] )
  (findTransformation (Point 0 ((-1)%2)) (Point 1 ((-1)%2)) (Point 0 (1%2)))
  
testFindTransformRotate = TestCase $ assertEqual
  "kruchu verchu naebat hochu"
  (fromList 3 3 [0, 0, 0, 0, 3%5, (-4)%5, 0, 4%5, 3%5] )
  (findTransformation (Point 0 0) (Point (3%5) ((-4)%5)) (Point (4%5) (3%5)))
  
testBreakEdgesHorizontal = TestCase $ assertEqual
    "opa opa"
    (breakEdges
        (map (uncurry Edge)
            [ (0, 2)
            ]
        )
        (map (uncurry Point)
            [ (0/1, 0/1)
            , (0/1, 1/1)
            , (0/1, 2/1)
            ]
        )
    )
    (map (uncurry Edge)
        [ (0, 1)
        , (1, 2)
        ]
    )

testBreakEdgesVertical = TestCase $ assertEqual
    "opa opa"
    (breakEdges
        (map (uncurry Edge)
            [ (0, 1)
            ]
        )
        (map (uncurry Point)
            [ (0/1, 0/1)
            , (2/1, 0/1)
            , (1/1, 0/1)
            ]
        )
    )
    (map (uncurry Edge)
        [ (0, 2)
        , (2, 1)
        ]
    )

testBreakEdgesProblem9 = TestCase $ assertEqual
    "opa opa"
    (breakEdges
        (map (uncurry Edge)
            [ (0, 1)
            , (0, 3)
            , (1, 2)
            , (4, 5)
            , (3, 2)
            ]
        )
        (map (uncurry Point)
            [ (0/1, 0/1)
            , (1/2, 0/1)
            , (1/2, 2/3)
            , (0/1, 2/3)
            , (0/1, 1/3)
            , (1/2, 1/3)
            ]
        )
    )
    (map (uncurry Edge)
        [ (0, 1)
        , (0, 4)
        , (4, 3)
        , (1, 5)
        , (5, 2)
        , (4, 5)
        , (3, 2)
        ]
    )

tests = TestList 
  [ TestLabel "testDistance1" testDistance1
  , TestLabel "testDistance2" testDistance2
  , TestLabel "testMakeEdge" testMakeEdge
  , TestLabel "testSlice" testSlice
  , TestLabel "testSquareArea" testSquareArea
  , TestLabel "testTriangleArea" testTriangleArea
  , TestLabel "testSquareConvex" testSquareConvex
  , TestLabel "testTriangleConvex" testTriangleConvex
  , TestLabel "testArrowNonConvex" testArrowNonConvex
  , TestLabel "testNonConvex" testNonConvex
  , TestLabel "testPossibleTriangleFolds" testPossibleTriangleFolds
  , TestLabel "testGrahamScan" testGrahamScan
  , TestLabel "testGrahamScanSquare" testGrahamScanSquare
  , TestLabel "testGrahamScanOnEdge" testGrahamScanOnEdge
  , TestLabel "testMirrorX" testMirrorX
  , TestLabel "testMirrorY" testMirrorY
  , TestLabel "testMirrorDiagonal" testMirrorDiagonal
  , TestLabel "testMirrorSlope" testMirrorSlope
  , TestLabel "testCalculateOneFacet" testCalculateOneFacet
  , TestLabel "testCalculateSimpleFacets" testCalculateSimpleFacets
  , TestLabel "testCalculateHalfSquareFacets" testCalculateHalfSquareFacets
  , TestLabel "testNotOnEdgeLine" testNotOnEdgeLine
  , TestLabel "testFindTransformShift" testFindTransformShift
  , TestLabel "testFindTransformRotate" testFindTransformRotate
  , TestLabel "testBreakEdgesHorizontal" testBreakEdgesHorizontal
  , TestLabel "testBreakEdgesVertical" testBreakEdgesVertical
  , TestLabel "testBreakEdgesProblem9" testBreakEdgesProblem9
  ]

main = defaultMainWithOpts
       (hUnitTestToTests tests)
       mempty

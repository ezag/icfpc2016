module Origami where

import Data.List
import Data.List.Extras.Argmax
import Data.Function (on)
import Types
import Geometry
import GrahamScan
import qualified Data.Graph as G
import Data.Array
import Control.Monad
import Data.Maybe
import Debug.Trace


solve :: OrigamiDescription -> SolutionDescription
solve = makeSolution . flattenOrigami . makeOrigami

flattenOrigami :: Origami -> Origami
flattenOrigami or =
  maybe
    (error "zinochka is helpless")
    id
    (zinochka' [(or, 0)])

zinochka' ::  [(Origami, Int)] -> Maybe Origami
zinochka' [] = Nothing
zinochka' ((or,d):xs) = 
  if isFlat or
  then Just or
  else if d > maxDepth
    then Nothing
    else zinochka' (xs ++ (zip allOrigamis  (repeat (d+1))))
      where
        allFoldedOrigamis = map (applyFold or) (possibleFolds or)
        allOrigamis = sortBy comparator $ filter (\o -> (score or o) >= 0.0) allFoldedOrigamis
        comparator :: Origami -> Origami -> Ordering
        comparator or1 or2 = compare (score or or2) (score or or1)
        maxDepth = 5
        
zinochka :: Int -> Origami -> Maybe Origami
zinochka d or = --(trace ("ZINA: " ++ show or)) $
  if isFlat or
  then Just or
  else 
    if null allOrigamis || d > maxDepth
    then Nothing
    else join $ find isJust (map (zinochka (d + 1)) allOrigamis)
      where
        allFoldedOrigamis = map (applyFold or) (possibleFolds or)
        allOrigamis = sortBy comparator $ filter (\o -> (score or o) >= 0.0) allFoldedOrigamis
        comparator :: Origami -> Origami -> Ordering
        comparator or1 or2 = compare (score or or2) (score or or1)
        maxDepth = 5



score :: Origami -> Origami -> Float
score (Origami orps _ _ _) (Origami ps es _ _) = -- (trace ("SCORE: " ++ show ps ++ "  " ++ show [area1, distless, bigger])) $
  if not $ and [area1, distless, bigger]
  then -1.0
  else ar
    where
      area1 = ar <= 1 + eps
      distless = all (<= sqrt 2 + eps) pairwiseDists
      pairwiseDists = map (uncurry distance) [(i, j) | i <- ps, j <- ps, i < j]
      bigger = not $ isEnvelope orps ps

      ar = area (grahamScan ps)
      edgeLengths = map segmentLen $ map (\e -> edgeToSegment e ps) es
      segmentLen (Segment x y) = distance x y


possibleFolds :: Origami -> [Fold]
possibleFolds (Origami _ edges facets _) =
  concatMap getEdgeFolds edges
  where
    getAdjacentFacets e =
      filter (\f -> isAdjacent f e) facets
    getEdgeFolds e = map (Fold e) (getAdjacentFacets e)


applyFold :: Origami -> Fold -> Origami
applyFold (Origami ps es fs meta) fld = 
  Origami newps newes newfs (updateCorrespondence meta correspondence)
  where
    newps = {-(trace ("HU: " ++ show mirrored ++ " --- " ++ show fld)) $-} ps ++ mirrored
    newes = union es (mirrorEdges fld es correspondence)
    newfs = calculateFacets newps newes
    (mirrored, correspondence) = mirrorPoints ps fld


mirrorPoints :: [Point] -> Fold -> ([Point], [(Int, Int)])
mirrorPoints ps (Fold e f) = (newps, correspondence)
  where
    indices  = getMirrorIndices ps e f
    correspondence = zip indices ([length ps..])
    newps = map (mirror segment) (map (ps!!) indices)
    segment = edgeToSegment e ps

edgeToSegment :: Edge -> [Point] -> Segment
edgeToSegment (Edge i j) ps =
  if i < length ps && j < length ps
  then Segment (ps !! i) (ps !! j)
  else error "edgeToSegment: point index too large"

mirrorEdges :: Fold -> [Edge]-> [(Int, Int)] -> [Edge]
mirrorEdges (Fold e f) edges corr =
  map mapIndex edges
  where
    mapIndex (Edge i j) = Edge (replace i) (replace j)
    replace i = case find (\x -> fst x == i) corr of
      Just (x, y) -> y
      Nothing -> i
  
    
getMirrorIndices :: [Point] -> Edge -> Facet -> [Int]
getMirrorIndices ps e@(Edge m n) (Facet es) =
    filter (\i -> isOnSameHalfPlane seg notOnEdge (ps!!i)) allNotOnEdge
    where
      getEdgePointList (Edge i j) = [i, j]
      allNotOnEdge = filter (notOnEdgeLine e ps) $ nub $ concatMap getEdgePointList es
      notOnEdge =
        if null allNotOnEdge
        then error "getMirrorIndices: no points outside fold edge"
        else ps !! (head allNotOnEdge)
      seg = edgeToSegment e ps

  

notOnEdgeLine :: Edge -> [Point] -> Int -> Bool
notOnEdgeLine e ps i = not $ isOnLine seg (ps !! i)
  where
    seg = edgeToSegment e ps


makeSolution :: Origami -> SolutionDescription
makeSolution (Origami pts edges _ (Meta iss)) = -- (trace (show points)) $
  SolutionDescription points solfacets destPoints
  where
    (po, ph, pv) = basisFromFlat pts
    lt = findTransformation po ph pv
    points = map (applyTransformation lt) pts
    solfacets = foldr (\f sfs -> (makeSolutionFacet f points): sfs ) [] (minimalFacets pts edges)
    destPoints = map (pts!!) destIndices
    destIndices = map findInMeta [0..(length pts - 1)]
    findInMeta i =
      maybe
        (error "makeSolution: can't find index")
        head
        (find (elem i) iss)

basisFromFlat :: [Point] -> (Point, Point, Point)
basisFromFlat ps = (conv !! 2, conv !! 1, conv !! 3)
  where
    conv = grahamScan ps

isFlat :: Origami -> Bool
isFlat (Origami ps es _ _) = and [convex4, area1]
  where
    convex4 = (length (grahamScan ps) == 4)
    area1 = (convArea <=  1.01 && convArea >= 0.99)
    convArea = area $ grahamScan ps


makeOrigami :: OrigamiDescription -> Origami
makeOrigami (OrigamiDescription pols ss) = (trace ("MAKE: " ++ show ss ++ "\n" ++ show res)) $ res
  where
    res = Origami points edges facets meta
    pPoints = foldr (\pol pts -> union (getPoints pol) pts) [] pols
    sPoints = union
        (map (\(Segment f _) -> f) ss)
        (map (\(Segment _ s) -> s) ss)
    skeletonIntersections = map fromJust $ filter isJust maybeSkeletonIntersections
    maybeSkeletonIntersections = map (uncurry getIntersection) segmentPairs
    segmentPairs = [(s1, s2) | s1 <- ss, s2 <- ss, s1 /= s2]
    points = union (union pPoints sPoints) skeletonIntersections
    edges = breakEdges (getEdges points ss) points
    facets = calculateFacets points edges
    meta = Meta $ map (:[]) [0..(length points - 1)]

minimalFacets :: [Point] -> [Edge] -> [Facet]
minimalFacets ps es = (trace ("MIN: " ++ show allCycles ++ "\nRES: " ++ show resCycles)) $
  map makeFacet resCycles
  where
    --resCycles = filter (isNotEnveloping minCycles) minCycles
    resCycles = filter (isNotEnveloping allCycles) allCycles
    isNotEnveloping :: [[Int]] -> [Int] -> Bool
    isNotEnveloping iss is = --(trace ("INE: " ++ show iss ++ " " ++ show is ++ " " ++ show ( (length $ filter (isEnvelopeByIndices ps is) iss) == 1 ))) $
      (length $ filter (isEnvelopeByIndices ps is) iss) == 1
    --minCycles = filter (isMinimal allCycles) allCycles
    allCycles = getVertexCycles ps es
    isMinimal :: [[Int]] -> [Int] -> Bool
    isMinimal iss is = (length $ filter (isSubList is) iss) == 1
    isSubList :: [Int] -> [Int] -> Bool
    isSubList ds ss = (union ss ds) == ds

isEnvelopeByIndices :: [Point] -> [Int] -> [Int] -> Bool
isEnvelopeByIndices ps big small = --(trace ("I1: " ++ show big ++ " " ++ show small ++ " " ++ show res)) $
  res
  where
    res = isEnvelope bigp smallp
    bigp = map (ps!!) big
    smallp = map (ps!!) small

isEnvelope :: [Point] -> [Point] -> Bool
isEnvelope big small = --trace ("MS: " ++ (show midSegments)) $
  all (==True) $ map (isInsidePoly big) (small ++ midSegments)
  where
    midSegments = zipWith midPoint small (tail small ++ [head small])

calculateFacets :: [Point] -> [Edge] -> [Facet]
calculateFacets ps es = map makeFacet $ getVertexCycles ps es

makeFacet :: [Int] -> Facet
makeFacet l = Facet $ zipWith Edge l (tail l ++ [head l])

getVertexCycles :: [Point] -> [Edge] -> [[Int]]
getVertexCycles ps es = --(trace (show (cycles graph))) $
  cycles graph
  where
    graph = G.buildG bounds pairs
    pairs = concatMap (\(Edge i j) -> [(i, j), (j,i)]) es
    maxPoint = maximum $ concatMap (\(Edge i j) -> [i, j]) es
    bounds = (0, maxPoint)
    

cycles :: G.Graph -> [[Int]]
cycles g =
  nubBy (\l1 l2 -> sort l1 == sort l2) (filter (\x -> length x > 2) rawCycles)
  where
    rawCycles = concatMap cycles' (G.vertices g)
    cycles' v = build [] v v
    build p s v =
      let p' = p ++ [v]
          local = [p' | x <- (g ! v), x == s]
          good w = w > s && not (w `elem` p')
          ws = filter good (g ! v)
          extensions = concatMap (build p' s) ws
      in local ++ extensions

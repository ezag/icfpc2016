module Geometry where

import Types
import Data.Ratio
import Data.List
import Debug.Trace
import Data.Matrix hiding (trace)

distance :: Point -> Point -> Float
distance (Point x y) (Point z t)  = sqrt $ fromRational $ (x - z)^2 + (y - t)^2

area :: [Point] -> Float
area points = 0.5 * (fromRational $ abs $ sum $ zipWith determinant points (tail points ++ [head points]))

determinant :: Point -> Point -> Rational
determinant (Point x y) (Point z t) = x * t - y * z 

isConvex :: [Point] -> Bool
isConvex points = --(trace ("IC: " ++ show points ++ " " ++ show signs ++ " " ++ show res)) $
  res
  where
    firstNotZero =
      maybe
        (error "isConvex: all points are collinear")
        id
        (find (/= 0) signs)
    res = notAllZero && all (\x -> (x == firstNotZero || x == 0)) (tail signs)
    notAllZero = not $ all (==0) signs
    shift1 = (tail points ++ [head points])
    shift2 = (tail shift1 ++ [head shift1])
    zcross (Point x1 y1) (Point x2 y2) (Point x3 y3) =
      (x2 - x1) * (y3 - y2) - (y2 - y1) * (x3 - x2)
    signs = map signum $ zipWith3 zcross points shift1 shift2

midPoint :: Point -> Point -> Point
midPoint (Point x y) (Point z t) = Point ((x + z)/2) ((y + t)/2)

isInsidePoly :: [Point] -> Point -> Bool
isInsidePoly ps p = --(trace ("IIP: " ++ show ps ++ " ? " ++ show p ++ " " ++ show [onEdge, rayTest])) $
  onEdge || rayTest 
  where
    segments = zipWith Segment ps (tail ps ++ [head ps])
    onEdge = any (==True) $ map (isInsideSegment p) segments
    rayTest = odd $ length $ filter (==True) $ map (intersectWithRay p) (zip3 segments seg_1 seg_2)
    seg_1 = tail segments ++ [head segments]
    seg_2 = tail seg_1 ++ [head seg_1]
      
-- test with ray (y = y3, x > x3)
intersectWithRay :: Point -> (Segment, Segment, Segment) -> Bool
intersectWithRay (Point x3 y3) ((Segment ppp@(Point xp1 yp1) (Point xp2 yp2)), (Segment (Point x1 y1) (Point x2 y2)), (Segment ppa@(Point xa1 ya1) (Point xa2 ya2)))
  | (y1 == y2) = (y3 == y1 && (x3 - x1) * (x3 - x2) < 0)
  | otherwise = (xi > x3) && ((y3 == y1) && (not $ isOnSameHalfPlane (Segment (Point xi y3) (Point x3 y3)) ppp ppa) || (y3 - y1) * (y3 - y2) < 0)
  where
    b = (x2 - x1) / (y1 - y2)
    c = -x1 - b * y1
    xi = -c - b * y3


-- not entirely correct
getIntersection :: Segment -> Segment -> Maybe Point
getIntersection s1@(Segment (Point x1 y1) (Point x2 y2)) s2@(Segment (Point x3 y3) (Point x4 y4))
  | (y1 == y2) && (y3 == y4) = Nothing
  | (y1 == y2) = findResult (Point (-e*y1 - f) y1)
  | (y3 == y4) = findResult (Point (-b*y3 - c) y3)
  | b == e    = if (c /= f) then Nothing else Nothing
  | otherwise = findResult intersection
  where
    b = (x2 - x1) / (y1 - y2)
    c = -x1 - b*y1
    e = (x4 - x3) / (y3 - y4)
    f = -x3 - e*y3
    yi = (f - c) / (b - e) 
    xi = -c -b*yi
    intersection = Point xi yi
    inside p = isInsideSegment p s1 && isInsideSegment p s2
    findResult p = if inside p then Just p else Nothing 
    
-- prereq: p1 and p2 are not on segment line
isOnSameHalfPlane :: Segment -> Point -> Point -> Bool
isOnSameHalfPlane (Segment (Point x1 y1) (Point x2 y2)) (Point x3 y3) (Point x4 y4)
  | (x1 == x2) = (x3 - x1) * (x4 - x1) >= 0
  | (y1 == y2) = (y3 - y1) * (y4 - y1) >= 0
  | otherwise = (x3 + b*y3 + c) * (x4 + b*y4 + c) >= 0
  where
    b = (x2 - x1) / (y1 - y2)
    c = -x1 - b*y1

  
isInsideSegment :: Point -> Segment -> Bool
isInsideSegment p (Segment p1@(Point x1 y1) p2@(Point x2 y2)) =
  distance p p1 + distance p p2 <= distance p1 p2 + eps

eps :: Float
eps = 0.0001

mirror :: Segment -> Point -> Point
mirror (Segment (Point x1 y1) (Point x2 y2)) (Point x3 y3)
  | (x1 == x2) = (Point (2 * x1 - x3) y3)
  | (y1 == y2) = (Point x3 (2 * y1 - y3))
  | otherwise = (Point (2 * x_int - x3) (2 * y_int - y3))
    where
      a = (y1 - y2) / (x1 - x2)
      c = y1 - a * x1
      d = y3 + x3 / a
      x_int = (d - c) / (a + 1 / a)
      y_int = d - x_int / a

isOnLine :: Segment -> Point -> Bool
isOnLine (Segment (Point x1 y1) (Point x2 y2)) (Point x3 y3)
  | (x1 == x3) = (x1 == x2) || (y1 == y3)
  | (x2 == x3) = (x1 == x2) || (y2 == y3)
  | otherwise = (y3 - y2)/(x3 - x2) == (y3 - y1)/(x3 - x1)

findTransformation :: Point -> Point -> Point -> Matrix Rational
findTransformation (Point ox oy) (Point hx hy) (Point vx vy) = final_mat
  where
    rotMat = (identity 2) * inverse2 (fromList 2 2 [hx - ox, vx - ox, hy - oy, vy - oy])
    rx1 = getElem 1 1 rotMat
    rx2 = getElem 1 2 rotMat
    ry1 = getElem 2 1 rotMat
    ry2 = getElem 2 2 rotMat
    final_mat = fromList 3 3 [0, 0, 0, sx, rx1, rx2, sy, ry1, ry2]
    sx = - rx1 * ox - rx2 * oy
    sy = - ry1 * ox - ry2 * oy
    

inverse2 :: Matrix Rational -> Matrix Rational
inverse2 mat = fromList 2 2 [d / dnm, (-b) / dnm, (-c) / dnm, a / dnm]
  where
    a = getElem 1 1 mat 
    b = getElem 1 2 mat 
    c = getElem 2 1 mat 
    d = getElem 2 2 mat 
    dnm = a * d - b * c


applyTransformation :: Matrix Rational -> Point -> Point
applyTransformation mat (Point x y) = res -- (trace ("Mat: " ++ show mat ++ "\nPT: " ++ show x ++ " ; " ++ show y)) $ res
  where
    dest = mat * (fromList 3 1 [1, x, y])
    res = Point (resultList !! 1) (resultList !! 2)
    resultList = toList dest


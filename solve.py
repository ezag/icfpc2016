import argparse
import json
import os
import sys
import subprocess
import time


class ArgumentParser(argparse.ArgumentParser):
    def __init__(self):
        super(ArgumentParser, self).__init__()
        self.add_argument('--report', action='store_true')
        self.add_argument('--only-download', action='store_true')
        self.add_argument('--no-download', action='store_true')
        self.add_argument('--skip-interrupted', action='store_true')


class Problem(object):
    def __init__(self, id_):
        self.n = int(id_)

    def make_filename(self, suffix):
        return 'problems/{:>06}{}'.format(self.n, suffix)

    @property
    def id_(self):
        return self.n

    @property
    def description(self):
        return self.make_filename('-problem.txt')

    @property
    def dl_stderr(self):
        return self.make_filename('-dl-stderr.txt')

    @property
    def solution(self):
        return self.make_filename('-solution.txt')

    @property
    def solve_stderr(self):
        return self.make_filename('-stderr.txt')

    @property
    def old_solution(self):
        return self.make_filename('-solution-old.txt')

    @property
    def interrupted(self):
        return self.make_filename('.INTERRUPTED')

    @property
    def submit(self):
        return self.make_filename('-submit.json')

    @property
    def submit_stderr(self):
        return self.make_filename('-submit-stderr.txt')

    @property
    def submit_json(self):
        try:
            with open(self.submit) as f:
                j = json.load(f)
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            return None
        return j


def solve(args):
    with open('problem_spec_hashes_ids.txt') as f:
        hashes_ids = [l.split() for l in f.readlines()]
    for p_hash, p_id in hashes_ids:
        p = Problem(p_id)
        print("Processing problem_id={} ({})".format(p.id_, p_hash))
        j = p.submit_json
        if j is not None and j['ok'] and j['resemblance'] == 1.0:
            print(" already solved")
            continue
        print("  checking desciption...", end='', flush=True)
        need_to_download = not args.no_download
        have_downloaded = False
        need_timeout = False
        if os.path.exists(p.description):
            with open(p.description) as f:
                body = f.read()
                if body:
                    print(' already downloaded')
                    need_to_download = False
                    have_downloaded = True
                else:
                    print(' (previous download failed)...', end='', flush=True)
        else:
            print(' downloading...', end='', flush=True)
        if need_to_download:
            so = open(p.description, 'w')
            se = open(p.dl_stderr, 'w')
            r = subprocess.run([
                    './origami',
                    'get',
                    'blob/{}'.format(p_hash),
                ],
                stdout=so,
                stderr=se
            )
            so.close()
            se.close()
            if r.returncode != 0:
                print(' failed, returncode={}'.format(r.returncode))
                continue
            print(' ok')
            have_downloaded = True
        if args.only_download:
            continue
        if not have_downloaded:
            print(' not available')
            continue
        old_found = False
        if os.path.exists(p.solution):
            print('  old solution found, renaming')
            old_found = True
            os.rename(p.solution, p.old_solution)
        print("  solving...", end='', flush=True)
        if args.skip_interrupted:
            if os.path.exists(p.interrupted):
                print(' have been interrupted, skipping')
                continue
        so = open(p.solution, 'w')
        se = open(p.solve_stderr, 'w')
        try:
            r = subprocess.run([
                    './origami',
                    'solvefromfile',
                    p.description,
                ],
                stdout=so,
                stderr=se,
                timeout=7,
            )
        except KeyboardInterrupt:
            print(' interrupted by user')
            with open(p.interrupted, 'w'):
                pass
            continue
        except subprocess.TimeoutExpired:
            print(' timed out')
            with open(p.interrupted, 'w'):
                pass
            continue
        else:
            if os.path.exists(p.interrupted):
                os.remove(p.interrupted)
        finally:
            so.close()
            se.close()
        if r.returncode != 0:
            print(' failed, returncode={}'.format(r.returncode))
            continue
        print(' ok')
        should_submit = True
        if old_found:
            print(' comparing with old solution...', end='', flush=True)
            r = subprocess.run([
                    'cmp',
                    '-s',
                    p.solution,
                    p.old_solution,
                ],
            )
            if r.returncode == 0:
                print(' not changed')
                if p.submit_json is not None:
                    should_submit = False
            print('  differs')
        if not should_submit:
            continue
        print("  submitting...", end='', flush=True)
        so = open(p.submit, 'w')
        se = open(p.submit_stderr, 'w')
        need_timeout = True
        r = subprocess.run([
                'curl',
                '--compressed',
                '-L',
                '-H',
                'Expect:',
                '-H',
                'X-API-Key: 91-bc2e3af7c23611f3bb0988019b484f30',
                '-F',
                'problem_id={}'.format(p.id_),
                '-F',
                'solution_spec=@{}'.format(p.solution),
                'http://2016sv.icfpcontest.org/api/solution/submit',
            ],
            stdout=so,
            stderr=se,
        )
        so.close()
        se.close()
        if r.returncode != 0:
            print(' failed, returncode={}'.format(r.returncode))
            continue
        j = p.submit_json
        if j['ok']:
            print(' ok {}'.format(j['resemblance']))
        else:
            print(' error: {}'.format(j['error']))
        if need_timeout:
            time.sleep(1)


def report(args):
    ls = os.listdir('problems')
    ps = [Problem(int(n[:6])) for n in ls if n.endswith('-problem.txt')]
    print('Report for {} problems'.format(len(ps)))
    for p in ps:
        print('{:>6} '.format(p.id_), end='')
        if os.path.exists(p.interrupted):
            print('interrupted')
            continue
        if not os.path.exists(p.solution):
            print('unattempted')
            continue
        with open(p.solution) as f:
            solution = f.read()
        if not solution:
            print('fail: ', end='')
            nonempty = False
            with open(p.solve_stderr) as f:
                for l in f.readlines():
                    if l.startswith('origami: '):
                        nonempty = True
                        print(l[len('origami: '):].strip())
            if not nonempty:
                print('(interrupted?)')
            continue
        j = p.submit_json
        if not j:
            print('not submitted')
            continue
        if j['ok']:
            print('ok: {}'.format(j['resemblance']))
        else:
            print('error: {}'.format(j['error']))


if __name__ == '__main__':
    args = ArgumentParser().parse_args()
    if not args.report:
        solve(args)
    else:
        report(args)

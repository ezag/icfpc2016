module GrahamScan(grahamScan) where

import Data.Function (on)
import Data.List (sortBy)
import Prelude hiding (Either(..))
import Types
import Debug.Trace

data Turn = Left | Right | Colinear
            deriving (Show, Eq)




px (Point x1 _) = x1
py (Point _ y1) = y1


grahamScan :: [Point] -> [Point]
grahamScan points
    | length points < 3 = error "Degenerate"
    | otherwise         = 
        --let (firstPoint:rest) = (trace ("===" ++ show points)) $ (findFirstPoint points)
        let (firstPoint:rest) = (findFirstPoint points)
            sortedRest = sortBy (compare `on` (angle firstPoint)) rest

            --loop (a:as) (b:c:ps) = (trace ("TT: " ++ show a ++ " " ++ show b ++ " " ++ show c ++ " " ++ show (turn a b c))) $ case turn a b c of
            loop (a:as) (b:c:ps) = case turn a b c of
                                Right      -> loop as (a:c:ps)
                                Left       -> loop (b:a:as) (c:ps)
                                Colinear   -> loop (a:as) (c:ps)
            loop (a:xs) [c] = case turn a c firstPoint of
                                Left       -> (c:a:xs)
                                _          -> (a:xs)

        --in (trace ("---" ++ show (loop [firstPoint] sortedRest))) $ (loop [firstPoint] sortedRest)
        in loop [firstPoint] sortedRest

findFirstPoint points
    | null points = error "Null points"
    | otherwise   = loop points [] where
    loop (a:[]) ps = a:ps
    loop (a:b:rest) ps =
        if (py a, px a) < (py b, px b)
        then loop (a:rest) (b:ps)
        else loop (b:rest) (a:ps)

angle :: Point -> Point -> (Float, Float)
angle a b = (atan2 (dy) (dx), abs (dx))
  where
    dx = fromRational $ px b - px a
    dy = fromRational $ py b - py a

turn a b c = case compare cross 0 of
               GT -> Left
               EQ -> Colinear
               LT -> Right
    where
      cross = x1 * y2 - x2 * y1
      x1 = px b - px a
      y1 = py b - py a
      x2 = px c - px a
      y2 = py c - py a

module Types where

import Data.List
import Data.Ord
import Data.Ratio
import Data.String.Utils
import Debug.Trace
import Control.Monad
import Control.Monad.State

data OrigamiDescription = OrigamiDescription [Polygon] [Segment]
  deriving (Show)

data Polygon = Polygon [Point]
  deriving (Show)

data Segment = Segment Point Point
  deriving (Show, Eq)

data Origami = Origami [Point] [Edge] [Facet] Meta
  deriving (Show)

data Meta = Meta [[Int]]
  deriving (Show)

data Point = Point Rational Rational
  deriving (Eq, Ord)

data Edge = Edge Int Int
  deriving (Show, Eq)

data Facet = Facet [Edge]
  deriving (Show, Eq)

data SolutionDescription = SolutionDescription [Point] [SolutionFacet] [Point]

data SolutionFacet = SolutionFacet [Int]

data Fold = Fold Edge Facet
  deriving (Show, Eq)

getPoints :: Polygon -> [Point]
getPoints (Polygon ps) = ps

getEdges ::  [Point] -> [Segment] -> [Edge]
getEdges ps ss =
  map (\s -> makeEdge s ps) ss

makeEdge :: Segment -> [Point] -> Edge
makeEdge (Segment x y) vs = Edge xnum ynum
  where
    findP p = 
      case (findIndex (==p) vs) of
        Just i -> i
        Nothing -> error ("makeEdge: can't find point " ++ show p)
    xnum = findP x
    ynum = findP y

distance' :: Point -> Point -> Float
distance' (Point x y) (Point z t)  = sqrt $ fromRational $ (x - z)^2 + (y - t)^2

isInsideSegment' :: Point -> Segment -> Bool
isInsideSegment' p@(Point x0 y0) (Segment p1@(Point x1 y1) p2@(Point x2 y2)) =
  distance' p p1 + distance' p p2 <= distance' p1 p2 + eps
  where
    eps = 0.0001

segmentFromEdge :: Edge -> [Point] -> Segment
segmentFromEdge (Edge i j) points =
    Segment (points !! i) (points !! j)

breakEdges :: [Edge] -> [Point] -> [Edge]
breakEdges edges points =
    concatMap breakEdge edges
    where
        breakEdge :: Edge -> [Edge]
        breakEdge edge =
            map
                (flip makeEdge points)
                ((segmentsFromPoints.orderPoints.breakingPoints) edge)

        breakingPoints :: Edge -> [Point]
        breakingPoints edge =
            filter
                ((flip isInsideSegment') (segmentFromEdge edge points))
                points

        orderPoints :: [Point] -> [Point]
        orderPoints pts =
            sortBy (comparing cmpCoord) pts
            where
                Point x1 y1 = head pts
                Point x2 y2 = (head.tail) pts
                cmpCoord = if x1 == x2
                    then \(Point _ y) -> y
                    else \(Point x _) -> x

        segmentsFromPoints :: [Point] -> [Segment]
        segmentsFromPoints pts =
            map (uncurry Segment) (zip pts (tail pts))

makeSolutionFacet :: Facet -> [Point] -> SolutionFacet
makeSolutionFacet (Facet edges) vertices = SolutionFacet pts
  where
    pts = nub $ foldr (\(Edge m n) is -> m:n:is) [] edges

instance Show Point where
  show (Point x y) =
    show (numerator x) ++ "/" ++ show (denominator x) ++ "," ++
    show (numerator y) ++ "/" ++ show (denominator y)

instance Show SolutionFacet where
  show (SolutionFacet pts) = show (length pts) ++ " " ++ (intercalate " " (map show pts))

instance Show SolutionDescription where
  show (SolutionDescription points facets newPoints) =
    show (length points) ++ "\n" ++
    intercalate "\n" (map show points) ++ "\n" ++
    show (length facets) ++ "\n" ++
    intercalate "\n" (map show facets) ++ "\n" ++
    intercalate "\n" (map show newPoints)


getNext :: State [String] String
getNext = state $ \(x:xs) -> (x,xs)  


readOrigamiDesription :: String -> OrigamiDescription 
readOrigamiDesription s = evalState readOrigamiDescriptionM $ lines s

readPolygonM :: State [String] Polygon
readPolygonM = do
  numVertices <- liftM (readInt) $ getNext
  vertices <- replicateM numVertices (liftM readPoint $ getNext)
  return $ Polygon vertices

readSegmentM :: State [String] Segment
readSegmentM = do
  s <- getNext
  return $ readSegment s


readOrigamiDescriptionM :: State [String] OrigamiDescription
readOrigamiDescriptionM = do
  numPolygons <- liftM (readInt) $ getNext
  polygons <- replicateM numPolygons readPolygonM
  numSegments <- liftM (readInt) $ getNext
  segments <- replicateM numSegments readSegmentM
  return $ OrigamiDescription polygons segments


readInt :: String -> Int
readInt = read 

readPoint :: String -> Point
readPoint s = Point x y
  where 
    parts = split "," s
    x = readRational (head parts)
    y = readRational (parts !! 1)

readRational :: String -> Rational
readRational s =
  case (findIndex (=='/') s) of
    Just i -> (read (slice s 0 i) :: Integer) % (read (slice s (i + 1) (length s)) :: Integer)
    Nothing -> (read s :: Integer) % 1

readSegment :: String -> Segment 
readSegment s = Segment p1 p2
  where
    p1 = readPoint $ head $ words s
    p2 = readPoint $ (words s) !! 1

slice :: [a] -> Int -> Int -> [a]
slice as l r = take (r - l) . drop l $ as
      
isAdjacent :: Facet -> Edge -> Bool
isAdjacent (Facet es) (Edge i j) = elem (Edge i j) es || elem (Edge j i) es

updateCorrespondence :: Meta -> [(Int,Int)] -> Meta
updateCorrespondence (Meta iss) corr = res
  where
    res = Meta $ foldr updateOne iss corr
    updateOne :: (Int, Int) -> [[Int]] -> [[Int]]
    updateOne pair iss = map (updateOneCorr pair) iss

    updateOneCorr :: (Int, Int) -> [Int] -> [Int]
    updateOneCorr (f, s) is = 
      if elem f is
      then is ++ [s]
      else is
  

#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import fileinput
from fractions import Fraction
import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import sys
import numpy as np

from matplotlib.widgets import Button



if len(sys.argv) != 2:
  print "Expected one argument, which is a trace file"


lines = [line.rstrip('\n') for line in open(sys.argv[1])]

currentLine = 0

def readFloat(s):
  return float(Fraction(s))

def nextLine():
  global currentLine
  l = lines[currentLine]
  currentLine+=1
  return l

minX = None
maxX = None
minY = None
maxY = None
num_facets = int(nextLine())
facets = []
for i in range(0, num_facets):
  num_vertices = int(nextLine())
  facet = []
  for j in range(0, num_vertices):
    foo = nextLine().split(",")
    x = readFloat(foo[0])
    y = readFloat(foo[1])
    if (minY == None) or (y < minY):
      minY = y
    if (minX == None) or (x < minX):
      minX = x
    if (maxY == None) or (y > maxY):
      maxY = y
    if (maxX == None) or (x > maxX):
      maxX = x
    facet.append((x,y))
  facets.append(facet)

minX -= 0.2
minY -= 0.2
maxX += 0.2
maxY += 0.2

segments = []
num_segments = int(nextLine())
for i in range(0, num_segments):
  foo = nextLine().split(" ")
  s1 = foo[0].split(",")
  s2 = foo[1].split(",")
  x1 = readFloat(s1[0])
  y1 = readFloat(s1[1])
  x2 = readFloat(s2[0])
  y2 = readFloat(s2[1])
  segments.append([[x1,y1], [x2,y2]])

print segments

#### draw facets:
plt.figure(figsize=(8,8), dpi=80)
ax = plt.subplot(1, 1, 1)
ax.set_xticks(np.arange(minX, maxX,0.1))
ax.set_yticks(np.arange(minY, maxY,0.1))


patches = []


for i in range(num_facets):
    polygon = Polygon(facets[i], True)
    patches.append(polygon)

p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.3)

colors = 100*np.random.rand(len(patches))
p.set_array(np.array(colors))

ax.add_collection(p)



for s in segments:
  #ax.axvline(x, color='r')
  xx = [s[0][0], s[1][0]]
  yy = [s[0][1], s[1][1]]
  ax.plot(xx, yy, color='k', linestyle='-', linewidth=2)


plt.gca().set_aspect('equal', adjustable='box')
#ax.plot([0.0, 1.0, 1.0, 0.0], [0.0, 0.0, 1.0, 1.0], color='k', linestyle='-', linewidth=2)
#x.axvline([[0.0, 0.0], [1.0, 0.0]], color='r')
ax.set_xlim(minX, maxX)
ax.set_ylim(minY, maxY)
#ax.autoscale_view(True,True,True)



## x1 = [x[0] for x in source_vertices]
## y1 = [x[1] for x in source_vertices]
## print x1
## print y1
#
##plt.figure(1)
##
##plt.subplot(221)
##plt.plot(x1, y1)
##plt.yscale('linear')
##plt.title('linear')
##plt.grid(True)

plt.grid()
plt.show()
import numpy as np
import matplotlib.pyplot as plt
import fileinput
from fractions import Fraction
import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import sys

from matplotlib.widgets import Button



if len(sys.argv) != 2:
  print "Expected one argument, which is a trace file"



class Solution:
  def __init__(self, source_vertices, facets, destination_vertices):
    self.source_vertices = source_vertices
    self.facets = facets
    self.destination_vertices = destination_vertices



solutions = []

lines = [line.rstrip('\n') for line in open(sys.argv[1])]

currentLine = 0

def readFloat(s):
  return float(Fraction(s))

def nextLine():
  global currentLine
  l = lines[currentLine]
  currentLine+=1
  return l

def readSolutions():
  while(currentLine < len(lines)):
    l = nextLine()
    if l == "zalupa_konskaya":
      readNextSolution()

def readNextSolution():
  global solutions
  num_vertices = int(nextLine())
  source_vertices = []
  for i in range(0, num_vertices):
    foo = nextLine().split(",")
    print foo
    x = readFloat(foo[0])
    y = readFloat(foo[1])
    source_vertices.append((x,y))
  
  num_facets = int(nextLine())
  facets = []
  for i in range(0, num_facets):
    foo = nextLine().split()
    n_vertices = int(foo[0])
    facet = []
    for j in range(0, n_vertices):
      facet.append(int(foo[j+1]))
    facets.append(facet)
  
  destination_vertices = []
  for i in range(0, num_vertices):
    foo = nextLine().split(",")
    x = readFloat(foo[0])
    y = readFloat(foo[1])
    destination_vertices.append((x,y))
  solutions.append(Solution(source_vertices, facets, destination_vertices))
  print "success"


readSolutions()

fig1 = plt.figure(figsize=(10,10), dpi=80)

colors = 100*np.random.rand(1000)

def drawSolution(solution):
  
  
  ## 
  
  
  ax = plt.subplot(2, 2, 1, aspect='equal')
  #ax = fig1.add_axes([0.55, 0.1, 0.4, 0.4])
  
  plt.cla()
  
  patches = []
  
  
  for i in range(len(solution.facets)):
      facet = solution.facets[i]
      poly_coords = [ solution.source_vertices[n] for n in facet ]
      polygon = Polygon(poly_coords, True)
      patches.append(polygon)
  
  p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.3)
  
  p.set_array(np.array(colors))
  
  ax.add_collection(p)
  
  
  #plt.figure(figsize=(8,8), dpi=80)
  ax = plt.subplot(2, 2, 2, aspect='equal')
  #ax = fig1.add_axes([0.1, 0.1, 0.4, 0.4])
  
  plt.cla()
  patches = []
  
  
  for i in range(len(solution.facets)):
      facet = solution.facets[i]
      poly_coords = [ solution.destination_vertices[n] for n in facet ]
      polygon = Polygon(poly_coords, True)
      patches.append(polygon)
  
  p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.3)
  
  
  p.set_array(np.array(colors))
  
  ax.add_collection(p)
  
  

# make up some data in the interval ]0, 1[





class Index(object):
    ind = 0

    def next(self, event):
        global solutions
        if self.ind < len(solutions) - 1:
          self.ind += 1
         
          drawSolution(solutions[self.ind])
       

    def prev(self, event):
        global solutions
        if self.ind > 0:
          self.ind -= 1
          
          drawSolution(solutions[self.ind])

callback = Index()

axprev = plt.axes([0.7, 0.05, 0.1, 0.075])
axnext = plt.axes([0.81, 0.05, 0.1, 0.075])
bnext = Button(axnext, 'Next')
print dir(bnext)
bnext.on_clicked(callback.next)
bprev = Button(axprev, 'Previous')
bprev.on_clicked(callback.prev)


## x1 = [x[0] for x in source_vertices]
## y1 = [x[1] for x in source_vertices]
## print x1
## print y1
#
##plt.figure(1)
##
##plt.subplot(221)
##plt.plot(x1, y1)
##plt.yscale('linear')
##plt.title('linear')
##plt.grid(True)


drawSolution(solutions[0])
plt.show()
#!/usr/bin/env python
import re
import numpy as np
import matplotlib.pyplot as plt
import fileinput
from fractions import Fraction
import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import sys
import numpy as np

from matplotlib.widgets import Button

def pairwise(it):
    it = iter(it)
    while True:
        yield next(it), next(it)

def readOrigami(s):
  oStart = s.find("Origami") + 7
  pstart = s.find('[', oStart) + 1
  pend = s.find(']')
  pointsStr = s[pstart:pend]

  xs = [ float(Fraction(x)) for x in pointsStr.split(",")]
  
  points = [ [a,b] for (a,b) in pairwise(xs)]
  
  
  edgeEnd= s.find(']', pend)
  
  facets = []
  nextFacetStart = s.find('Facet')
  while nextFacetStart != -1:
    nextFacetStart+=5
    facet = []
    edgesStart = s.find('[', nextFacetStart)
    edgesEnd = s.find(']', nextFacetStart)
    xs = s[edgesStart+1:edgesEnd].split(",")
    for i in range(0, len(xs)):
      m = re.search('Edge (\d+) (\d+)', xs[i])
      facet.append((points[int(m.group(1))], points[int(m.group(2))]))
    facets.append(facet)
    nextFacetStart = s.find('Facet', nextFacetStart)
  return facets







if len(sys.argv) != 2:
  print "Expected one argument, which is a trace file"







solutions = []

lines = [line.rstrip('\n') for line in open(sys.argv[1])]




def readSolutions():
  for l in lines:
    if l.find("Origami") != -1:
      solutions.append(readOrigami(l))




readSolutions()

fig1 = plt.figure(figsize=(12,12), dpi=80)

colors = 100*np.random.rand(1000)

minX = None
maxX = None
minY = None
maxY = None

def drawSolution(solution):
  global minX,minY,maxX,maxY
  minX = None
  maxX = None
  minY = None
  maxY = None
  def minMaxPoint(p):
    global minX,minY,maxX,maxY
    x = p[0]
    y = p[1]
    if (minY == None) or (y < minY):
      minY = y
    if (minX == None) or (x < minX):
      minX = x
    if (maxY == None) or (y > maxY):
      maxY = y
    if (maxX == None) or (x > maxX):
      maxX = x

  for facet in solution:
     for edge in facet:
       minMaxPoint(edge[0])
       minMaxPoint(edge[1])

  minX -= 0.2
  minY -= 0.2
  maxX += 0.2
  maxY += 0.2

  
#  ax = plt.subplot(1, 1, 1, aspect='equal')
  ax = fig1.add_axes([0.05, 0.15, 0.85, 0.75], aspect='equal')

  
  
  plt.cla()
  ax.set_xticks(np.arange(minX, maxX,0.1))
  ax.set_yticks(np.arange(minY, maxY,0.1))
  
  patches = []
  
  
  for facet in solution:
      print "facet: ", facet
      for edge in facet:
        print "edge:", edge
        x1 = edge[0][0]
        x2 = edge[1][0]
        y1 = edge[0][1]
        y2 = edge[1][1]
        plt.plot([x1, x2], [y1, y2], color='k', linestyle='-', linewidth=2)
     # polygon = Polygon(facet, True)
     # patches.append(polygon)
  
  #p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.3)
  
  #p.set_array(np.array(colors))
  
  #ax.add_collection(p)

  #ax.autoscale_view(True,True,True)
  ax.set_xlim(minX, maxX)
  ax.set_ylim(minY, maxY)
  plt.grid()
  
  

# make up some data in the interval ]0, 1[





class Index(object):
    ind = 0

    def next(self, event):
        global solutions
        if self.ind < len(solutions) - 1:
          self.ind += 1
         
          drawSolution(solutions[self.ind])
       

    def prev(self, event):
        global solutions
        if self.ind > 0:
          self.ind -= 1
          
          drawSolution(solutions[self.ind])

callback = Index()

axprev = plt.axes([0.7, 0.05, 0.1, 0.075])
axnext = plt.axes([0.81, 0.05, 0.1, 0.075])
bnext = Button(axnext, 'Next')
print dir(bnext)
bnext.on_clicked(callback.next)
bprev = Button(axprev, 'Previous')
bprev.on_clicked(callback.prev)


## x1 = [x[0] for x in source_vertices]
## y1 = [x[1] for x in source_vertices]
## print x1
## print y1
#
##plt.figure(1)
##
##plt.subplot(221)
##plt.plot(x1, y1)
##plt.yscale('linear')
##plt.title('linear')
##plt.grid(True)


drawSolution(solutions[0])
plt.show()
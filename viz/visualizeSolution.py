#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt
import fileinput
from fractions import Fraction
import matplotlib
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import sys
import numpy as np

if len(sys.argv) != 2:
  print "Expected one argument, which is a trace file"


lines = [line.rstrip('\n') for line in open(sys.argv[1])]

currentLine = 0

def readFloat(s):
  return float(Fraction(s))

def nextLine():
  global currentLine
  l = lines[currentLine]
  currentLine+=1
  return l

minX = None
maxX = None
minY = None
maxY = None

def readFloat(s):
  return float(Fraction(s))

num_vertices = int(nextLine())

source_vertices = []
for i in range(0, num_vertices):
  foo = nextLine().split(",")
  x = readFloat(foo[0])
  y = readFloat(foo[1])
  source_vertices.append((x,y))

num_facets = int(nextLine())
facets = []
for i in range(0, num_facets):
  foo = nextLine().split()
  n_vertices = int(foo[0])
  facet = []
  for j in range(0, n_vertices):
    facet.append(int(foo[j+1]))
  facets.append(facet)

destination_vertices = []
for i in range(0, num_vertices):
  foo = nextLine().split(",")
  x = readFloat(foo[0])
  y = readFloat(foo[1])
  if (minY == None) or (y < minY):
    minY = y
  if (minX == None) or (x < minX):
    minX = x
  if (maxY == None) or (y > maxY):
    maxY = y
  if (maxX == None) or (x > maxX):
    maxX = x
  destination_vertices.append((x,y))

minX -= 0.2
minY -= 0.2
maxX += 0.2
maxY += 0.2

print "success"

# make up some data in the interval ]0, 1[


colors = 100*np.random.rand(len(facets))




## 
plt.figure(figsize=(8,8), dpi=80)
ax = plt.subplot(1, 2, 1, aspect='equal')



patches = []


for i in range(num_facets):
    facet = facets[i]
    poly_coords = [ source_vertices[n] for n in facet ]
    polygon = Polygon(poly_coords, True)
    patches.append(polygon)

p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.3)

colors = 100*np.random.rand(len(patches))
p.set_array(np.array(colors))

ax.add_collection(p)

ax.autoscale_view(True,True,True)
#plt.figure(figsize=(8,8), dpi=80)
ax = plt.subplot(1, 2, 2, aspect='equal')
ax.set_xticks(np.arange(minX, maxX,0.1))
ax.set_yticks(np.arange(minY, maxY,0.1))

patches = []


for i in range(num_facets):
    facet = facets[i]
    poly_coords = [ destination_vertices[n] for n in facet ]
    polygon = Polygon(poly_coords, True)
    patches.append(polygon)

p = PatchCollection(patches, cmap=matplotlib.cm.jet, alpha=0.3)


p.set_array(np.array(colors))

ax.add_collection(p)

ax.set_xlim(minX, maxX)
ax.set_ylim(minY, maxY)
plt.grid()

## x1 = [x[0] for x in source_vertices]
## y1 = [x[1] for x in source_vertices]
## print x1
## print y1
#
##plt.figure(1)
##
##plt.subplot(221)
##plt.plot(x1, y1)
##plt.yscale('linear')
##plt.title('linear')
##plt.grid(True)


plt.show()